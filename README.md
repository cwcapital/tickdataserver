Install

$ sudo apt-get update

$ sudo apt-get install postgresql

$ sudo apt-get install nodejs

$ sudo apt-get install npm

$ npm install

Run the file to create the bitmex and coinmarketcap databases:
./bin/createdb
./bin/coinmarketcapcreatedb

Run the file to create new schemas, e.g. for creating a bitmex XBTUSD schema, we would run:
./bin/createschema BITMEX XBTUSD
./bin/coinmarketcapcreateschema

Launch dev server:
npm start

POST requests to /market and /price expect in json format
