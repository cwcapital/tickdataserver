'use strict'

var promise = require('bluebird');
var moment = require('moment');
var _ = require('lodash');

var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var connectionString = 'postgres://jcole:jcole123@localhost:5432/tickdata';
var connectionString_2 = 'postgres://jcole:jcole123@localhost:5432/coinmarketcap';
var db = pgp(connectionString);
var db_coinmarketcap = pgp(connectionString_2);

function flattenJsonTickArray(arr) {
  var ticks = '[';
  arr.forEach(function(arr) {
    ticks += '['+arr['tstamp']+','+arr['bid']+','+arr['ask']+'],';
  })
  ticks = ticks.slice(0, -1);
  ticks += ';'
  return ticks;
}

function fetchTicks(req, res, next) {
  let exchange = req.query.exchange;
  let symbol = req.query.symbol;
  let prevTickCount = req.query.prevTickCount;
  let begin,end;

  if (req.query.to) {
    end = moment(req.query.to, 'YYYY-MM-DD-HH-mm-ss');
  } else {
    end = moment();
  }

  if (req.query.from) {
    begin = moment(req.query.from, 'YYYY-MM-DD-HH-mm-ss');
  } else {
    if (req.query.seconds_ago) {
      begin = moment(end).subtract(Number(req.query.seconds_ago)*1000);
    } else {
      res.status(400)
       .json({
         status: 'failed',
         data: [],
         message: 'Request must include either <seconds_ago> or <to> time parameters'
       });

       return;
    }
  }

  let table = exchange+'_'+symbol
  let begin_str = begin.format('YYYY-MM-DD HH:mm:ss');
  let end_str = end.format('YYYY-MM-DD HH:mm:ss');

  db.any("SELECT tstamp,bid,ask FROM " + table +
      " WHERE tstamp >= '" + begin_str + "' AND tstamp < '" + end_str + "' " +
      " ORDER BY tstamp;")
    .then(function(data) {

      let prev_tick_count = 120;
      let previous_ticks = null;

      if (prevTickCount) {
        previous_ticks = db.any("SELECT * FROM " +
              " ( " +
                  " SELECT tstamp,bid FROM " + table +
                  " WHERE tstamp < '" + begin_str + "' " +
                  " ORDER BY tstamp DESC " +
                  " LIMIT " + prevTickCount +
              " ) AS q ORDER BY tstamp ASC");
      }

      let count = data.length;
      let status = count > 0 ? 'success' : 'no_ticks';
      let min_bid=null, max_bid=null;

      if(count > 1) {
        min_bid=data[0].bid;
        max_bid=data[0].bid;
        for (let i=1; i<count; i++) {
          if (data[i].bid > max_bid)
           max_bid = data[i].bid;

          if (data[i].bid < min_bid)
           min_bid = data[i].bid
        }
      }

      if (previous_ticks) {
        previous_ticks.then(function(prev_data) {
          res.status(200)
            .json({
              status: status,
              data: data,
              extra: { bid: { min: min_bid, max: max_bid, prev: prev_data }},
              message: 'Retrieved ' + count + ' ticks from ' + begin_str + ' to ' + end_str
            });
        }).catch(function(err) {
          return next(err);
        })
      } else {
        res.status(200)
          .json({
            status: status,
            data: data,
            extra: { bid: { min: min_bid, max: max_bid }},
            message: 'Retrieved ' + count + ' ticks from ' + begin_str + ' to ' + end_str
          });
      }
    })
    .catch(function(err) {
      return next(err);
    });
}

function buildMarketUpdateQuery(coins, idMap)
{
  let insert_query = "INSERT INTO market (coin, tstamp, rank," +
          "daily_volume_usd, market_cap_usd, available_supply," +
          "percent_change_24h, percent_change_7d) VALUES ";

  for (let i=0; i<coins.length; i++)
  {
   let coin_lookup = idMap[coins[i]['id']];
   if (coin_lookup)
   {
     let coin_id = coin_lookup.id;
     let tstamp = moment.unix(coins[i].last_updated).format('YYYY-MM-DD HH:mm:ss');
     let available_supply = parseInt(coins[i].available_supply);
         available_supply = isNaN(available_supply) ? null : available_supply;

     insert_query += " ('" + coin_id + "', '" + tstamp + "', " +
       coins[i].rank + ", " + coins[i]['24h_volume_usd'] + ", " +
       coins[i].market_cap_usd + ", " + available_supply + ", " +
       coins[i].percent_change_24h + ", " + coins[i].percent_change_7d +
       "),"
   }
  }

  insert_query = insert_query.slice(0,-1) + ";";
  return insert_query
}

function buildPriceUpdateQuery(coins, idMap)
{
  let insert_query = "INSERT INTO price (coin, tstamp, price_usd) VALUES ";

  for (let i=0; i<coins.length; i++)
  {
   let tstamp = moment.unix(coins[i].last_updated).format('YYYY-MM-DD HH:mm:ss');
   let coin_lookup = idMap[coins[i]['id']];
   if (coin_lookup)
   {
     let coin_id = coin_lookup.id;

     insert_query += " ('" + coin_id + "', '" +
       tstamp + "', " + coins[i].price_usd + "),";
   }
  }

 insert_query = insert_query.slice(0,-1) + ";";
 return insert_query
}

function createNewCoins(coin_arr) {
  let insert_query = "INSERT INTO coin (site_id, symbol, total_supply)" +
      " VALUES ";
  let insert_query_initial_len = insert_query.length
  let ignore = [];

  for (let i=0; i<coin_arr.length; i++)
  {
    let total_supply = parseInt(coin_arr[i].total_supply);
        total_supply = isNaN(total_supply) ? null : total_supply;

    // if (!isNaN(total_supply)) {
      insert_query += " ('" + coin_arr[i].id + "', '" + coin_arr[i].symbol + "', " +
       total_supply + "),";
    // } else {
    //   ignore.push(coin_arr[i])
    // }
  }

  if (insert_query.length > insert_query_initial_len) {
    insert_query = insert_query.slice(0,-1) + ";";
    return [db_coinmarketcap.none(insert_query), ignore]
  } else {
    return [null, ignore]
  }

}

function insertCoinMarketCapPrices(req, res, next) {
  let kwargs = {}
  kwargs['table'] = 'price';
  kwargs['update_query'] = buildPriceUpdateQuery;

  genericCoinMarketCapTableUpdate(req, res, next, kwargs)
}

function insertCoinMarketCapMarketData(req, res, next) {
  let kwargs = {}
  kwargs['table'] = 'market';
  kwargs['update_query'] = buildMarketUpdateQuery;

  genericCoinMarketCapTableUpdate(req, res, next, kwargs)
}

function genericCoinMarketCapTableUpdate(req, res, next, kwargs) {
  let coin_req;
  if (typeof(req.body.data) == 'object') {
    coin_req = req.body.data;
  }
  else {
    coin_req = JSON.parse(req.body.data);
  }

  db_coinmarketcap.any("SELECT c.id, c.site_id, MAX(tstamp) \
                       FROM " + kwargs['table'] +
                       " JOIN coin c ON " + kwargs['table'] + ".coin = c.id \
                       GROUP BY c.id")
    .then(function(coins)
    {
      let site_id_map = _.keyBy(coins, function(o) { return o.site_id })
      let to_update = [];
      let ignore = [];
      let response_msg = '';

      for (let i=0; i<coin_req.length; i++)
      {
        let coin_lookup = site_id_map[coin_req[i].id]

        if (coin_lookup)
        {
          let db_last_tstamp = coin_lookup.max
          if (moment.unix(coin_req[i].last_updated).isAfter(db_last_tstamp))
          {
            to_update.push(coin_req[i])
          }
        } else {
          ignore.push(coin_req[i])
        }
      }

      if(to_update.length > 0 || ignore.length > 0)
      {
        if(to_update.length > 0) // Update server
        {
          let insert_query = kwargs['update_query'](to_update, site_id_map)
          db_coinmarketcap.none(insert_query)
            .then(function() {
              let update_str = _.join(_.map(to_update, (o) => o.id), ', ');
              let json_response;

              if (ignore.length == 0) {
                json_response = {
                  'status': 'success',
                  'message': 'successfully updated ' + to_update.length + ' coin(s): ' + update_str
                }
              } else {
                let warning_str = _.join(_.map(ignore, (o) => o.id), ', ');
                json_response = {
                  'status': 'warning',
                  'message': 'successfully updated ' + to_update.length + ' coin(s): ' + update_str,
                  'warning': 'the following coin(s) were not found and ignored: '
                                + warning_str
                }
              }
              res.status(200)
                .json(json_response)
            })
        } else { // Do not update server, but still show warning message
          let warning_str = _.join(_.map(ignore, (o) => o.id), ', ');
          res.status(200)
            .json({
            'status': 'warning',
            'warning': 'the following ' + ignore.length + ' coin(s) were not found and ignored: '
                          + warning_str
          })
        }
      } else {
        res.status(200)
         .json({
           status: 'failed',
           message: 'database not updated'
         })
      }
    })
    .catch(function(err) {
      next(err)
    })
}

function insertCoinMarketCapCoins(req, res, next) {
  let coin_req;
  if (typeof(req.body.data) == 'object') {
    coin_req = req.body.data;
  }
  else {
    coin_req = JSON.parse(req.body.data);
  }

  db_coinmarketcap.any("SELECT site_id from coin")
    .then(function(coins)
    {
      let email_body = '';
      let new_coins = [];

      for(let i=0; i<coin_req.length; i++)
      {
        if (_.findIndex(coins, function(o) { return o.site_id == coin_req[i].id; }) < 0)
        {
          email_body += 'Creating new coin: ' + coin_req[i].id + '\n';
          new_coins.push(coin_req[i])
        }
      }

      let arr = createNewCoins(new_coins);
      let DBUpdate = arr[0];
      let ignore = arr[1];

      if (DBUpdate)
      {
         DBUpdate
          .then(function()
          {
            // db_coinmarketcap.none()
            db_coinmarketcap.any("SELECT site_id, id FROM coin")
             .then(function(data)
             {
               let idMap = _.keyBy(data, function(o) { return o.site_id })
               let price_query = buildPriceUpdateQuery(new_coins, idMap);
               let market_query = buildMarketUpdateQuery(new_coins, idMap);

               Promise.all([
                 db_coinmarketcap.none(price_query),
                 db_coinmarketcap.none(market_query)
               ]).then(function() {
                 let new_coins_str = _.join(_.map(new_coins, (o) => o.id), ', ');
                 if (ignore.length == 0) {
                   res.status(200)
                     .json({
                       status: 'success',
                       message: 'created new coin(s): ' + new_coins_str
                     })
                 } else {
                   let warning_str = _.join(_.map(ignore, (o) => o.id), ', ');
                   res.status(200)
                     .json({
                       status: 'warning',
                       message: 'created new coin(s): ' + new_coins_str,
                       'warning': 'the following coin(s) were not found and ignored: '
                                     + warning_str
                     })
                 }
               }).catch(function(err) {
                 return next(err)
               })
             }) //end then()
             .catch(function(err) {
               return next(err)
             })
          })
          .catch(function(err) {
            return next(err)
          })
      } else {
      res.status(200)
       .json({
         status: 'failed',
         message: 'no new coins created'
       })
      }
    })
    .catch(function(err) {
      return next(err)
    })
}

function fetchCoinMarketCapMarketData(req, res, next)
{
  if (req.query.symbol)
  {
    db_coinmarketcap.any("SELECT * FROM market \
                           JOIN coin ON coin.id = market.coin \
                           WHERE coin.site_id = '" + req.query.symbol + "' \
                           ORDER BY tstamp")
     .then(function(data) {
       res.status(200).
        json({
          'status': 'success',
          'data': data
        })
     })
  } else {
    res.status(400).
     json({
       'status': 'failed',
       'error': 'request does not include symbol in query string'
     })
  }
}

function fetchCoinMarketCapPrices(req, res, next)
{
  if (req.query.symbol)
  {
    db_coinmarketcap.any("SELECT tstamp, price_usd FROM price \
                           JOIN coin ON coin.id = price.coin \
                           WHERE coin.site_id = '" + req.query.symbol + "' \
                           ORDER BY tstamp")
     .then(function(data) {
       res.status(200).
        json({
          'status': 'success',
          'data': data
        })
     })
  } else {
    res.status(400).
     json({
       'status': 'failed',
       'error': 'request does not include symbol in query string'
     })
  }
}

function fetchCoinMarketCapAllCoins(req, res, next)
{
  db_coinmarketcap.any("SELECT * from coin")
   .then(function(data) {
     res.status(200)
      .json({
        status: 'success',
        data: data
      })
   })
}

function insertTicks(req, res, next) {
  let exchange = req.body.exchange;
  let symbol = req.body.symbol;
  let table = exchange+'_'+symbol;

  let ticks = req.body.data.split(';');

  let vals = '';
  for (let i=0; i<ticks.length-1; i++) {
    let ti = ticks[i].split(',');
    vals += "('"+ ti[0] + "',"+ ti[1] +","+ ti[2] + "),";
  }
  vals = vals.slice(0,-1);

  db.none('INSERT INTO ' + table + '(tstamp, bid, ask)' +
      ' VALUES ' + vals + ';')
    .then(function() {
      res.status(200)
        .json({
          status: 'success',
          message: 'Inserted ' + (Number(ticks.length)-1) + ' tick(s)'
        })
    })
    .catch(function(err) {
      return next(err)
    })
}

module.exports = {
  fetchTicks: fetchTicks,
  insertTicks: insertTicks,
  fetchCoins: fetchCoinMarketCapAllCoins,
  insertCoinMarketCapCoins: insertCoinMarketCapCoins,
  fetchCoinMarketCapPrices: fetchCoinMarketCapPrices,
  insertCoinMarketCapPrices: insertCoinMarketCapPrices,
  fetchCoinMarketCapMarketData: fetchCoinMarketCapMarketData,
  insertCoinMarketCapMarketData: insertCoinMarketCapMarketData
};
