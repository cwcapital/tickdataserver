var express = require('express');
var router = express.Router();

var db = require('../queries')

router.get('/api/ticks', db.fetchTicks);
router.post('/api/ticks', db.insertTicks);
router.get('/api/coinmarketcap', db.fetchCoins);
router.post('/api/coinmarketcap', db.insertCoinMarketCapCoins);
router.get('/api/coinmarketcap/prices', db.fetchCoinMarketCapPrices);
router.post('/api/coinmarketcap/prices', db.insertCoinMarketCapPrices);
router.get('/api/coinmarketcap/market', db.fetchCoinMarketCapMarketData);
router.post('/api/coinmarketcap/market', db.insertCoinMarketCapMarketData);

module.exports = router;
